package application;

import java.net.URL;
import java.nio.file.Paths;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class FXMLText extends Application {
	//main
	public static void main(String[] args) {
		//lancamos la aplicacion
		Application.launch(FXMLText.class, args);
	}

	@Override
	public void start(Stage arg0) throws Exception {
		arg0.setTitle("Hola a todos...");
		arg0.centerOnScreen();
		
		//URL de net
		//creamos una url al arxivo
		URL file = Paths.get("src\\application\\myfxml.fxml").toUri().toURL();
		//cargamos el arxivo
		Parent path = FXMLLoader.load(file);
		
		//creamos una scena a partir de nuestro fxml file, y le damos un tama�o por defecto
		Scene myScene = new Scene(path, 600, 500);
		//a�adimos la scena a nuestro arg0
        arg0.setScene(myScene);
        
        //mostramos la scena
        arg0.show();
        
        arg0.setResizable(false);
        
        //Thread.sleep(1000);
        
        //cerramos la ventana
        //arg0.close();
		
	}
	
}
