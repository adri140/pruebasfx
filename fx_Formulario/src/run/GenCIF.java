package run;

import java.util.Random;

public class GenCIF {
	
	private static int num = 10000000;
	
	private static char randomChar() {
		Random rnd = new Random();
		return  (char)((rnd.nextInt(25) + 1) + 64);
	}
	
	public static String getCif() {
		String cif = num + "" + (char) (randomChar());
		num++;
		return cif;
	}
}
