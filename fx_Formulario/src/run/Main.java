package run;

import clases.*;

public class Main {
	
	public static void main(String[] args) {
		Persona[] pers = genPersona(50);
		for(Persona pe: pers) {
			System.out.println(pe);
		}
	}
	
	public static Persona[] genPersona(int max) {
		Persona[] pers = new Persona[max];
		RandomName e = new RandomName();
		for(int i = 0; i < pers.length; i++) {
			String name = e.getName();
			if(i < pers.length - 1) {
				pers[i] = new Cliente(GenCIF.getCif(), "14051998", name.split(" ")[0], name.split(" ")[1], i);
			}
			else pers[i] = new Admin(GenCIF.getCif(), "14051998", name.split(" ")[0], name.split(" ")[1], i);
		}
		return pers;
	}
}
