package clases;

public class Cliente extends Persona {
	
	private Double money;
	
	public Cliente(String cif, String password, String name, String surname, Integer age) {
		super(cif, password, name, surname, age);
		this.money = 10.99;
	}
	
	public Double getMoney() {
		return this.money;
	}
	
	public void setMoney(Double mon) {
		this.money += mon;
	}
	
	@Override
	public String toString() {
		return super.toString() + " money: " + this.getMoney();
	}
}
