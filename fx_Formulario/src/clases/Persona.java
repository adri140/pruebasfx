package clases;

public abstract class Persona {
	
	private String CIF;
	private String password;
	private String name;
	private String surname;
	private Integer age;
	private boolean active;
	
	public Persona(String cif, String password) {
		this.CIF = cif;
		this.password = password;
		this.active = true;
	}
	
	public Persona(String cif, String password, String name, String surname, Integer age) {
		this(cif, password);
		this.name = name;
		this.surname = surname;
		this.setAge(age);
	}

	public String getCIF() {
		return CIF;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		if(age > 10) this.age = age;
		else this.age = 10;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((CIF == null) ? 0 : CIF.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Persona other = (Persona) obj;
		if (CIF == null) {
			if (other.CIF != null)
				return false;
		} else if (!CIF.equals(other.CIF))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "name: " + this.getName() + " surname: " + this.getSurname() + " cif: " + this.getCIF() + " age: " + this.getAge();
	}
}
