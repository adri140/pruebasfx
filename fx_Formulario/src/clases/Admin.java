package clases;

public class Admin extends Persona {
	
	private boolean permission;
	
	public Admin(String cif, String password, String name, String surname, Integer age) {
		super(cif, password, name, surname, age);
		this.permission = true;
	}

	public boolean isPermission() {
		return permission;
	}

	public void setPermission(boolean permission) {
		this.permission = permission;
	}
	
	@Override
	public String toString() {
		return super.toString() + " permission: " + this.isPermission();
	}
}
