package application;
	
import java.net.URL;
import java.nio.file.Paths;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;

/*https://www.youtube.com/watch?v=OT_qBWKiRfY*/
public class Main extends Application {
	
	private Controler control;
	
	@Override
	public void start(Stage primaryStage) {
		try {
			primaryStage.setResizable(false);
			primaryStage.setTitle("Productor");
			
			URL file = Paths.get("src\\application\\window.fxml").toUri().toURL();
			
			Scene scene = new Scene(FXMLLoader.load(file),950,500);
			scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
			
			primaryStage.setScene(scene);
			primaryStage.show();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
