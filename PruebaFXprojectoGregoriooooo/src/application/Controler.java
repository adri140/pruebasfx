package application;

import java.net.URL;
import java.util.ResourceBundle;

import clases.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

public class Controler implements Initializable {
	
	@FXML private TextField idProduFilter;
	/*Esto hay que mirar-lo*/
	@FXML private RadioButton todosFilter;
	@FXML private RadioButton VendibleFilter;
	@FXML private RadioButton IngreFilter;
	@FXML private CheckBox trencaStockFilter;
	@FXML private Button refreshButton;
	
	
	//creamos la tabla con la clase que mostrara, ojo con el nombre de estas ya que hacen referencia a la id de la tabla en la ventana
	@FXML private TableView<Product> tablaProdu;
	//igual para cada una de las columnas
	@FXML private TableColumn idProColumn;
	@FXML private TableColumn nameProColumn;
	@FXML private TableColumn desProColumn;
	@FXML private TableColumn preuVnedaProColumn;
	@FXML private TableColumn totalStockProColumn;
	@FXML private TableColumn minStockProColumn;
	@FXML private TableColumn mesuraUnityProColumn;
	@FXML private TableColumn tipusProColumn;
	@FXML private TableColumn idProveidorProColumn;
	//contiene los objetos a mostrar
	ObservableList<Product> productos;
	
	public Controler() {
		
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		this.iniciarTabla();
		/*final ObservableList<Product> tablaProductosSel = tablaProdu.getSelectionModel().getSelectedItems();
		tablaProductosSel.addL*/
		
		Provider pp = new Provider("jamonero", "ES339033", null);
		//a�ade productos, en un futuro extraer de su almacen...
		for(int i = 0; i < 10; i++) {
			Product p = new Product("Jamon" + i, UnityMeasure.UNITAT, 10);
			p.setProveidor(pp);
			productos.add(p);
		}
		
	}
	
	/*@FXML
	private void insert(ActionEvent event) {
	
	}*/
	
	private void iniciarTabla() {
		idProColumn.setCellValueFactory(new PropertyValueFactory<Product, Integer>("IdProduct"));
		nameProColumn.setCellValueFactory(new PropertyValueFactory<Product, String>("nameProduct"));
		//desProColumn.setCellValueFactory(new PropertyValueFactory<Product, String>("null"));
		preuVnedaProColumn.setCellValueFactory(new PropertyValueFactory<Product, Double>("price"));
		
		totalStockProColumn.setCellValueFactory(new PropertyValueFactory<Product, Integer>("stock"));
		minStockProColumn.setCellValueFactory(new PropertyValueFactory<Product, Integer>("minStock"));
		mesuraUnityProColumn.setCellValueFactory(new PropertyValueFactory<Product, UnityMeasure>("unitat"));
		tipusProColumn.setCellValueFactory(new PropertyValueFactory<Product, Types>("type"));
		
		//no funciona
		idProveidorProColumn.setCellValueFactory(new PropertyValueFactory<Product, Provider>("proveidor"));
		
		productos = FXCollections.observableArrayList();
		tablaProdu.setItems(productos);
	}

}
