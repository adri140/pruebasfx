package queries;

import java.util.Comparator;

public interface Sortable<T>
{
	public void sortBy(Comparator<? super T>... sortCriteria);

	public void sort();
}
