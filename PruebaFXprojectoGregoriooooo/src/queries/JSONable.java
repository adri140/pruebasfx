package queries;

public interface JSONable
{
	public void fromJSON(Object json);

	public Object toJSON();
}
