package queries;

import java.util.List;
import queries.Condition;
import queries.Filter;

public interface Queryable<T>
{
	public List<T> query(Condition<T>... conditions);

	public List<T> query(Filter<T> filter);
}
