package clases;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import queries.Condition;

public class Product implements Comparable<Product>
{
	public Integer idProduct;
	public String nameProduct;
	public Integer stock;
	public Integer minStock;
	public UnityMeasure unitat;
	public Map<Product, Integer> composicio;
	public List<Batch> lots;
	public Types type;
	public Provider proveidor;
	public double price; // preu Venda
	public double pes; // peso
	public String description;

	public void afegirLot(int quantitat, Date dataCaducitat)
	{
		int qLot = Generator.getNextLot();
		lots.add(new Batch(qLot, dataCaducitat, quantitat));
	}

	public String veureLots()
	{
		String cadena = "";
		for (Batch ld : lots)
		{
			cadena += "   " + ld + "\n";
		}
		return cadena;
	}

	public String getNameProduct()
	{
		return this.nameProduct;
	}

	public void setNameProduct(String nameProduct)
	{
		this.nameProduct = nameProduct;
	}

	@Override
	public String toString()
	{
		String cadena =
				"Producte: " + idProduct + "\t - " + nameProduct + "\tStock Total: " + getStock() + " " + unitat;
		cadena = cadena + "\tStockM�nim:" + minStock + "\t" + this.type;
		return cadena;
	}

	public Product()
	{
		this.idProduct = Generator.getNextProducte();
		lots = new ArrayList<Batch>();
		composicio = new HashMap<Product, Integer>();
		this.type = Types.INGREDIENT;
		minStock = 0;
		stock = 0;
	}

	public Product(String nomProducte)
	{
		this();
		this.nameProduct = nomProducte;
	}

	public Product(String nomProducte, UnityMeasure u, int sm)
	{
		this(nomProducte);
		this.setUnitatMesura(u);
		this.minStock = sm;
	};

	public void setType(Types type)
	{
		this.type = type;
	}

	public Types getType()
	{
		return this.type;
	}

	public void setProveidor(Provider pv)
	{
		this.proveidor = pv;
	}

	public Provider getProveidor()
	{
		return proveidor;
	}

	public UnityMeasure getUnitatMesura()
	{
		return unitat;
	}

	public void setUnitatMesura(UnityMeasure unitatm)
	{
		unitat = unitatm;
	}

	public void setStock(Integer q)
	{
		this.stock = q;
	}

	public void setMinStock(Integer stockM)
	{
		this.minStock = stockM;
	}

	public Integer getMinStock()
	{
		return this.minStock;
	}

	public void setPrice(double price)
	{
		this.price = price;
	}

	public double getPrice()
	{
		return price;
	}

	public Map<Product, Integer> getComposicio()
	{
		return composicio;
	}

	public void afegirComponent(Product p, int q)
	{
		composicio.put(p, q);
	}

	public String veureComposicio()
	{
		String cadena = "";
		Set<Product> claus = composicio.keySet();
		cadena = this.getNameProduct() + " --> ";
		for (Product p : claus)
			cadena += p.getNameProduct() + "(" + composicio.get(p) + ") ";
		return cadena;
	}

	@Override
	public int compareTo(Product p)
	{
		return (this.getNameProduct().compareTo(p.getNameProduct()));
	}

	public Integer getIdProduct()
	{
		return this.idProduct;
	}

	private Integer calclarStockLote()
	{
		int q = 0;
		for (Batch l : lots)
		{
			q += l.getQuantitat();
		}
		return q;
	}

	public Integer getStock()
	{
		return (stock + this.calclarStockLote());
	}

	public String viewLotsOrdenats()
	{
		lots.sort(null);
		String cadena = "";
		for (Batch ld : lots)
		{
			cadena += "   " + ld + "\n";
		}
		return cadena;
	}

	public List<Batch> getLots()
	{
		return this.lots;
	}

	public double getPes()
	{
		return this.pes;
	}

	/* Condiciones de product */
	/*
	 * A implementar: iqualID: id igual nameContainString: el nombre contiene este
	 * estring iqualStock: El producto tiene este estock moreStock: El producto
	 * tiene mas de este stock mostStock: El producto tiene menos de este stock
	 * iqualMinStock: El producto tiene este stock minimo moreMinStock lessMinStock
	 * iqualUnitat iqualType iqualProveidor iqualPrice morePrice lessPrice iqualPes
	 * morePes lessPes
	 */
	public final static Condition<Product> iqualId(Integer id)
	{
		Condition<Product> condition = new Condition<Product>(id)
		{
			@Override
			public boolean check(Product item)
			{
				if (((Integer) var[0]) == item.getIdProduct())
				{
					return true;
				}
				return false;
			}
		};
		return condition;
	}

	public final static Condition<Product> nameContainString(String data)
	{
		Condition<Product> condition = new Condition<Product>(data)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getNameProduct().toLowerCase().contains(data.toLowerCase()))
				{
					return true;
				}
				return false;
			}
		};
		return condition;
	}

	public final static Condition<Product> iqualStock(Integer stock)
	{
		Condition<Product> condition = new Condition<Product>(stock)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getStock() == stock)
					return true;
				return false;
			}
		};
		return condition;
	}

	public final static Condition<Product> iqualMinStock(Integer stock)
	{
		Condition<Product> condition = new Condition<Product>(stock)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getMinStock() == stock)
					return true;
				return false;
			}
		};
		return condition;
	}

	public final static Condition<Product> moreMinStock(Integer stock)
	{
		Condition<Product> condition = new Condition<Product>(stock)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getMinStock() > stock)
					return true;
				return false;
			}
		};
		return condition;
	}

	public final static Condition<Product> lessMinStock(Integer stock)
	{
		Condition<Product> condition = new Condition<Product>(stock)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getMinStock() < stock)
					return true;
				return false;
			}
		};
		return condition;
	}

	public final static Condition<Product> iqualUnitatMesura(UnityMeasure unitatm)
	{
		Condition<Product> condition = new Condition<Product>(unitatm)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getUnitatMesura() == unitatm)
					return true;
				return false;
			}
		};
		return condition;
	}

	public final static Condition<Product> iqualType(Types type)
	{
		Condition<Product> condition = new Condition<Product>(type)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getType() == type)
					return true;
				return false;
			}
		};
		return condition;
	}

	public final static Condition<Product> iqualProveidor(Provider pro)
	{
		Condition<Product> condition = new Condition<Product>(pro)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getProveidor().equals(pro))
					return true;
				return false;
			}
		};
		return condition;
	}

	public final static Condition<Product> iqualPrice(Double price)
	{
		Condition<Product> condition = new Condition<Product>(price)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getPrice() == price)
					return true;
				return false;
			}
		};
		return condition;
	}

	public final static Condition<Product> morePrice(Double price)
	{
		Condition<Product> condition = new Condition<Product>(price)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getPrice() > price)
					return true;
				return false;
			}
		};
		return condition;
	}

	public final static Condition<Product> lessPrice(Double price)
	{
		Condition<Product> condition = new Condition<Product>(price)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getPrice() < price)
					return true;
				return false;
			}
		};
		return condition;
	}

	public final static Condition<Product> iqualPes(Double pes)
	{
		Condition<Product> condition = new Condition<Product>(pes)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getPes() == pes)
					return true;
				return false;
			}
		};
		return condition;
	}

	public final static Condition<Product> morePes(Double pes)
	{
		Condition<Product> condition = new Condition<Product>(pes)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getPes() > pes)
					return true;
				return false;
			}
		};
		return condition;
	}

	public final static Condition<Product> lessPes(Double pes)
	{
		Condition<Product> condition = new Condition<Product>(pes)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getPes() < pes)
					return true;
				return false;
			}
		};
		return condition;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idProduct == null) ? 0 : idProduct.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (idProduct == null)
		{
			if (other.idProduct != null)
				return false;
		}
		else if (!idProduct.equals(other.idProduct))
			return false;
		return true;
	}

	/* fin condiciones de product */

}