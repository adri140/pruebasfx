package clases;

public class Generator
{
	private static Integer lotActual = 0;
	private static Integer producteActual = 0;
	private static Integer clientActual = 0;
	private static Integer proveidorActual = 0;
	private static Integer comandaActual = 0;

	public static int getNextLot()
	{
		return ++lotActual;
	}

	public static int getNextProducte()
	{
		return ++producteActual;
	}

	public static int getNextClient()
	{
		return ++clientActual;
	}

	public static int getNextComanda()
	{
		return ++comandaActual;
	}

	public static int getNextProveidor()
	{
		return ++proveidorActual;
	}
}
