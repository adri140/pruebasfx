package clases;

import java.util.Set;

import queries.Condition;

public class Provider
{
	private String nameProveidor;
	private String CIF;
	private boolean active;
	private ContactInfo contact;
	// nada

	public Provider()
	{
	}

	public Provider(String nameProveidor, String CIF, ContactInfo contact)
	{
		this();
		this.nameProveidor = nameProveidor;
		this.CIF = CIF;
		this.contact = contact;
	}

	public String getNameProveidor()
	{
		return this.nameProveidor;
	}

	public String getCIF()
	{
		return this.CIF;
	}

	public boolean isActive()
	{
		return this.active;
	}

	public void setActive(boolean active)
	{
		this.active = active;
	}

	public ContactInfo getContact()
	{
		return this.contact;
	}

	/*
	 * containName iqualCIF iqualActive
	 */

	public final static Condition<Provider> containName(String name)
	{
		Condition<Provider> condition = new Condition<Provider>(name)
		{
			@Override
			public boolean check(Provider item)
			{
				if (item.getNameProveidor().contains(name))
					return true;
				return false;
			}
		};
		return condition;
	}

	public final static Condition<Provider> iqualCIF(String CIF)
	{
		Condition<Provider> condition = new Condition<Provider>(CIF)
		{
			@Override
			public boolean check(Provider item)
			{
				if (item.getCIF().equals(CIF))
					return true;
				return false;
			}
		};
		return condition;
	}

	public final static Condition<Provider> iqualActive(boolean active)
	{
		Condition<Provider> condition = new Condition<Provider>(active)
		{
			@Override
			public boolean check(Provider item)
			{
				if (item.isActive() == active)
					return true;
				return false;
			}
		};
		return condition;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((CIF == null) ? 0 : CIF.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Provider other = (Provider) obj;
		if (CIF == null)
		{
			if (other.CIF != null)
				return false;
		}
		else if (!CIF.equals(other.CIF))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return this.getCIF();
	}
}